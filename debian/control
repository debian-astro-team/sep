Source: sep
Maintainer: Debian Astronomy Team <debian-astro-maintainers@lists.alioth.debian.org>
Uploaders: Ole Streicher <olebole@debian.org>,
           Phil Wyett <philip.wyett@kathenas.org>
Section: python
Priority: optional
Build-Depends: cmake,
               cython3,
               debhelper-compat (= 13),
               dh-sequence-python3,
               python3-all-dev,
               python3-astropy <!nocheck>,
               python3-numpy,
               python3-pytest <!nocheck>,
               python3-setuptools,
               python3-setuptools-scm
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/debian-astro-team/sep
Vcs-Git: https://salsa.debian.org/debian-astro-team/sep.git
Homepage: https://github.com/sep-developers/sep
Rules-Requires-Root: no

Package: libsep0
Architecture: any
Section: libdevel
Depends: ${misc:Depends},
         ${shlibs:Depends}
Multi-Arch: same
Description: C library for source extraction and photometry
 SEP makes the core algorithms of Source Extractor available as a
 library of stand-alone functions and classes. These operate
 directly on in-memory arrays (no FITS files or configuration files).
 The code is derived from the Source Extractor code base (written in
 C) and aims to produce results compatible with Source Extractor
 whenever possible.
 .
 This package contains the shared library.

Package: libsep-dev
Architecture: any
Section: libdevel
Depends: ${misc:Depends},
         ${shlibs:Depends},
         libsep0 (= ${binary:Version})
Description: C library for source extraction and photometry (development files)
 SEP makes the core algorithms of Source Extractor available as a
 library of stand-alone functions and classes. These operate
 directly on in-memory arrays (no FITS files or configuration files).
 The code is derived from the Source Extractor code base (written in
 C) and aims to produce results compatible with Source Extractor
 whenever possible.

Package: python3-sep
Architecture: any
Depends: ${misc:Depends},
         ${python3:Depends},
         ${shlibs:Depends},
         libsep0 (= ${binary:Version})
Description: Python library for source extraction and photometry
 SEP makes the core algorithms of Source Extractor available as a
 library of stand-alone functions and classes. These operate
 directly on in-memory arrays (no FITS files or configuration files).
 The code is derived from the Source Extractor code base (written in
 C) and aims to produce results compatible with Source Extractor
 whenever possible.
 .
 This Python module that wraps the C library in a Pythonic API. The
 Python wrapper operates on NumPy arrays.
